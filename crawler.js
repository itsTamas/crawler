var app, express, server, cons, swig, config, request, jsdom, url, fs;

express = require('express');
app = express();
cons = require('consolidate');
swig = require('swig');
config = require('./config.js');
request = require('request');
jsdom = require('jsdom');
url = require('url');
fs = require('fs');

// connecting mongoDB
var mongojs = require('mongojs');
var db = mongojs(config.mongoUrl);

// server config
app.configure(function(){
  app.use(express.bodyParser()); // posts
  app.use(express.cookieParser()); // cookies
  // logging
  app.use(express.logger({
    format: ' :req[ip] :date (:response-time ms): :method :url'
  }));
  app.use(express["static"]('./public')); // static files
  // errors
  app.use(function(err, req, res, next){
    console.error(err.stack);
    res.send(500, 'Something broke!');
  });
});

// render swig
app.engine('.html', cons.swig);
app.set('view engine', 'html');
swig.init({
  root: __dirname + '/views',
  autoescape: true,
  cache: config.env != 'development',
  encoding: 'utf8',
  allowErrors: true
});
app.set('views', __dirname + '/views');

// load routes
fs.readdirSync('./routes').forEach(function(file) {
    var name = file.substr(0, file.indexOf('.'));
    require('./routes/' + name)({
      app: app,
      db: db,
      request: request,
      jsdom: jsdom
    });
    console.log('Route loaded: ' + name + '.js');
});

// set server
server = app.listen(config.port);
console.log('App started...');