var lastitem = 20;

$(window).scroll(function(){

    if($(window).scrollTop() == $(document).height() - $(window).height()){

    	$('#load').css({"display":"inline"});
        AddMoreContent();
    }
});    

function AddMoreContent(){

	console.log(lastitem);
	var data = { last: lastitem };

	$.post('loadmore', data, function(result){

		//console.log(result[0]); // check result
		var new_divs;
		var length = result.length;
	
		for (var i = 0; i < length; i++) {

	  		var fun = result[i];
        	var div = '<div class="block"><a href='+fun.href+'><img style="background-image:url('+fun.image+');" height='+fun.height+'/></a></div>';
	  		new_divs += div;
	  	};

	  	lastitem = lastitem+20;
	  	$('body').append(new_divs);
      	$('#load').css({"display":"none"});
	  	setupBlocks();
	});
 }