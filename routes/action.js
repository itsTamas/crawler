module.exports = function($){

  // database
  var tumblr = $.db.collection("fromtumblr");

  // crawling
  function crawling(url){

    url = "http://anonymouse.org/cgi-bin/anon-www.cgi/http://www.tumblr.com/tagged/" + url;
        
    $.request({
          uri: url
      },
      function (err, response, body) {
        var self = this;
        self.items = new Array();
           
        // Error
        if (err && response.statusCode !== 200) { console.log('Error: req'); }

        $.jsdom.env({
            html: body,
            scripts: ['http://code.jquery.com/jquery-1.6.min.js']
        },
        function (err, window) {

          var $ = window.jQuery,
              $body = $('body'),
              $videos = $body.find('.photo');
              
              $videos.each(function (i, item) {

                var $image = $(item).find('.stage').attr('style'),
                    $width = $(item).attr('data-grow-width'),
                    $height = $(item).attr('data-grow-height'),
                    $link = $(item).find('a').attr('href');

                    // remove anonymouse from image link
                    $image = $image.substring(66);
                    $image = $image.substring(0, $image.length-3);

                    // remove anonymouse from link
                    $link = $link.substring(43);

                self.items[i] = {
                  image: $image,
                  width: $width,
                  height: $height,
                  href: $link
                };

                var items = self.items[i];
                
                tumblr.findOne(items, function(err, result){
                  if(!result){

                    // add to database if not exist + add date
                    items.date = new Date();

                    tumblr.insert(items, function (err, inserted) {
                      if(!err)
                        console.log('New data found and saved!');
                      else
                        console.log('Error while saving!');
                    });
                  };
                });

              });
              
          });
      });
  };

  $.app.get('/', function(req,res){

    crawling('funny');
    crawling('lol');
    crawling('fun');
    crawling('humour');
    crawling('humor');
    crawling('fail');
    crawling('joke');
    crawling('jokes');
    crawling('stupid');
    crawling('meme');
    crawling('memes');
  
    tumblr.find().sort({"_id":-1}).limit(20).toArray(function(err, data){
      if(!err)
        res.render('index',{data: data});
    });
  });

  $.app.post('/loadmore', function(req, res){

    var all = req.body.last;
    //console.log(all);

    tumblr.find().sort({"_id":-1}).skip((all*1)).limit(20).toArray(function(err, data){
      if(!err)
        res.send(data);
        //console.log(data);
    });

  });

};